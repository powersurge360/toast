from flask.ext.testing import TestCase

from toast import create_app


class ToastTestCase(TestCase):
    def create_app(self):
        return create_app(testing=True)

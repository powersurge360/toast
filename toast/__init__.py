from flask import Flask

from flask.ext.login import LoginManager
from flask.ext.mongoengine import MongoEngine

db = MongoEngine()
login_manager = LoginManager()


def create_app(testing=False):
    app = Flask("toast")

    if not testing:
        app.config.from_envvar("TOAST_SETTINGS")
    else:
        app.config.from_object("toast.test.settings")

    db.init_app(app)
    login_manager.setup_app(app)

    return app
